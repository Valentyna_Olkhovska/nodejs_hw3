const express = require('express');

const router = new express.Router();
const asyncHandler = require('express-async-handler');

const multer = require('multer');
const {
  userInfo,
  changePassword,
  deleteUser,
  uploadAvatar,
} = require('../services/usersService.js');
const { authMiddleware } = require('../middleware/authMiddleware');

const storage = multer.memoryStorage();
const upload = multer({ storage });

router.get('/', authMiddleware, asyncHandler(userInfo));

router.patch('/avatar', [authMiddleware, upload.single('avatar')], asyncHandler(uploadAvatar));

router.patch('/password', authMiddleware, asyncHandler(changePassword));

router.delete('/', authMiddleware, asyncHandler(deleteUser));

module.exports = {
  usersRouter: router,
};
