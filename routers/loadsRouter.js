const express = require('express');

const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  getUsersLoads,
  addLoad,
  getActivLoad,
  changeLoadState,
  getLoadById,
  updateLoad,
  deleteLoad,
  searchDriver,
  getLoadShippingInfo,
} = require('../services/loadsService');

const { authMiddleware } = require('../middleware/authMiddleware');
const { driverMiddleware } = require('../middleware/driverMiddleware');
const { shipperMiddleware } = require('../middleware/shipperMiddleware');

const { createdByMiddleware } = require('../middleware/createdByMiddleware');

router.get('/', authMiddleware, asyncHandler(getUsersLoads));

router.post('/', authMiddleware, shipperMiddleware, asyncHandler(addLoad));

router.get('/active', authMiddleware, driverMiddleware, asyncHandler(getActivLoad));

router.patch('/active/state', authMiddleware, driverMiddleware, asyncHandler(changeLoadState));

router.get('/:id', authMiddleware, asyncHandler(getLoadById));

router.put('/:id', authMiddleware, shipperMiddleware, asyncHandler(updateLoad));

router.delete('/:id', authMiddleware, shipperMiddleware, asyncHandler(deleteLoad));

router.post('/:id/post', authMiddleware, shipperMiddleware, createdByMiddleware, asyncHandler(searchDriver));

router.get('/:id/shipping_info', authMiddleware, shipperMiddleware, asyncHandler(getLoadShippingInfo));

module.exports = {
  loadsRouter: router,
};
