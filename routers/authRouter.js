const express = require('express');

const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  registerUser,
  loginUser,
  forgotPassword,
  resetPassword,
} = require('../services/authService.js');

router.post('/register', asyncHandler(registerUser));

router.post('/login', asyncHandler(loginUser));

router.post('/forgot_password', asyncHandler(forgotPassword)),

router.put('/reset_password/:token', asyncHandler(resetPassword));

module.exports = {
  authRouter: router,
};
