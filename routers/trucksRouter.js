const express = require('express');

const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  getUsersTrucks,
  addTrack,
  getUsersTrackById,
  updateTrack,
  deleteTruck,
  assignTruck,
} = require('../services/trucksService');

const { authMiddleware } = require('../middleware/authMiddleware');
const { driverMiddleware } = require('../middleware/driverMiddleware');
const { createdByMiddleware } = require('../middleware/createdByMiddleware');

router.get('/', authMiddleware, driverMiddleware, asyncHandler(getUsersTrucks));

router.post('/', authMiddleware, driverMiddleware, asyncHandler(addTrack));

router.get('/:id', authMiddleware, driverMiddleware, createdByMiddleware, asyncHandler(getUsersTrackById));

router.put('/:id', authMiddleware, driverMiddleware, createdByMiddleware, asyncHandler(updateTrack));

router.delete('/:id', authMiddleware, driverMiddleware, createdByMiddleware, asyncHandler(deleteTruck));

router.post('/:id/assign', authMiddleware, driverMiddleware, createdByMiddleware, asyncHandler(assignTruck));

module.exports = {
  trucksRouter: router,
};
