const express = require('express');
const morgan = require('morgan');

require('dotenv').config();

const app = express();

const cors = require('cors');

app.use(cors());

const mongoose = require('mongoose');

const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');
const { usersRouter } = require('./routers/usersRouter');
const { authRouter } = require('./routers/authRouter');

const {
  PORT,
  USER_NAME,
  USER_PASSWORD,
} = process.env;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/auth', authRouter);

const errorHandler = (err, req, res) => {
  console.error(err);
  if (Array.isArray(err.details)) {
    const { message } = err.details[0];

    if (message !== undefined && message !== null) {
      res.status(400).send({ message: `${message}` });
    }
  } else {
    res.status(500).send({ message: 'Server error' });
  }
};

app.use(errorHandler);

const start = async () => {
  try {
    await mongoose.connect(`mongodb+srv://${USER_NAME}:${USER_PASSWORD}@cluster0.zg9u1gp.mongodb.net/?retryWrites=true&w=majority`);
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
