const mongoose = require('mongoose');
const Joi = require('joi');

const UserJoiSchema = Joi.object({
  role: Joi.string()
    .valid('SHIPPER', 'DRIVER')
    .required(),

  password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
});

const AvatarJoiSchema = Joi.string().valid('image/jpeg', 'image/jpg', 'image/png');

const UserSchema = new mongoose.Schema({
  role: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: false,
  },

  createdDate: {
    type: String,
    required: false,
  },
});

UserSchema.methods.toJSON = function () {
  const obj = this.toObject();

  delete obj.password;
  delete obj.__v;

  return obj;
};

const User = mongoose.model('User', UserSchema);

module.exports = {
  UserJoiSchema,
  AvatarJoiSchema,
  User,
};
