const mongoose = require('mongoose');
const Joi = require('joi');

const TruckJoiSchema = Joi.object({
  type: Joi.string()
    .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),

  status: Joi.string()
    .valid('OL', 'IS'),

});

const TruckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: false,
  },
  createdDate: {
    type: String,
    required: false,
  },
});

TruckSchema.methods.toJSON = function () {
  const obj = this.toObject();

  delete obj.__v;

  return obj;
};

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = {
  TruckJoiSchema,
  Truck,
};
