const mongoose = require('mongoose');
const Joi = require('joi');

const LoadJoiSchema = Joi.object({

  name: Joi.string().min(3)
    .max(30)
    .required(),
  payload: Joi.number().min(1).max(4000).integer(),

  state: Joi.string()
    .valid('En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'),

  status: Joi.string()
    .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED')
    .required(),

  pickup_address: Joi.string()
    .min(3)
    .max(200)
    .required(),

  delivery_address: Joi.string()
    .min(3)
    .max(200)
    .required(),

  dimensions: Joi.object({
    width: Joi.number().min(1).max(700).integer(),
    length: Joi.number().min(1).max(350).integer(),
    height: Joi.number().min(1).max(200).integer(),
  }),
  logs: Joi.array().items({
    message: Joi.string(),
    time: Joi.string(),
  }),
});

const LoadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
  },

  logs: {
    type: [{}],
    required: false,
  },
  createdDate: {
    type: String,
    required: false,
  },
  count: {
    type: Number,
  },
});

LoadSchema.methods.toJSON = function () {
  const obj = this.toObject();

  delete obj.__v;

  return obj;
};

const Load = mongoose.model('Load', LoadSchema);

module.exports = {
  LoadJoiSchema,
  Load,
};
