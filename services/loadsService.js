const bcrypt = require('bcryptjs');
const { Types } = require('mongoose');
const { Load, LoadJoiSchema } = require('../models/Load.js');
const { Truck } = require('../models/Truck.js');
const { User } = require('../models/Users.js');

async function findTruck(width, length, height, payload) {
  let truck;
  if (width < 300 && length <= 250 && height <= 170 && payload <= 1700) {
    truck = await Truck.findOneAndUpdate({ type: 'SPRINTER', status: 'IS', assigned_to: { $exists: true } }, { status: 'OL' });

    return truck;
  }
  if (width > 500 || length > 250 || height > 170 || payload > 2500) {
    truck = await Truck.findOneAndUpdate({ type: 'LARGE STRAIGHT', status: 'IS', assigned_to: { $exists: true } }, { status: 'OL' });

    return truck;
  }

  if (width > 300 && length <= 250 && height <= 170 && payload <= 2500) {
    truck = await Truck.findOneAndUpdate({ type: 'SMALL STRAIGHT', status: 'IS', assigned_to: { $exists: true } }, { status: 'OL' });

    return truck;
  }

  return truck;
}

const getUsersLoads = async (req, res) => {
  const { userId } = req.user;
  const user = await User.findById(userId);
  const { role } = user;

  const { status, offset = 0, limit = 10 } = req.query;

  if (offset < 0) {
    res
      .status(400)
      .send({
        message: 'Offset should be greater than or equal zero',
      });

    return;
  }

  if (limit <= 0) {
    res
      .status(400)
      .send({
        message: 'Limit should be greater than zero',
      });

    return;
  }

  if (status && role === 'DRIVER') {
    const truck = await Truck.findOne({ assigned_to: userId });

    const loads = await Load.findOne({ assigned_to: truck._id, status }).skip(offset).limit(limit);
    const count = await Load.count({ assigned_to: truck._id });

    res
      .status(200)
      .send({
        loads,
        count,
      });

    return;
  }

  if (!status && role === 'DRIVER') {
    const truck = await Truck.findOne({ assigned_to: userId });

    const loads = await Load.findOne({ assigned_to: truck._id }).skip(offset).limit(limit);
    const count = await Load.count({ assigned_to: truck._id });

    res
      .status(200)
      .send({
        loads,
        count,
      });

    return;
  }
  if (!status) {
    const loads = await Load.find({ created_by: userId }).skip(offset).limit(limit);
    const count = await Load.count({ created_by: userId });

    res
      .status(200)
      .send({
        loads,
        count,
      });
    return;
  }
  const loads = await Load.find({ created_by: userId, status }).skip(offset).limit(limit);

  res
    .status(200)
    .send({
      loads,
    });
};

const addLoad = async (req, res) => {
  const { userId } = req.user;
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  const status = 'NEW';

  await LoadJoiSchema.validateAsync({
    name, payload, pickup_address, delivery_address, dimensions, status,
  });

  const load = new Load({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_by: userId,
    assigned_to: 'none',
    state: 'none',
    status,
    createdDate: new Types.ObjectId().getTimestamp().toISOString(),
  });

  try {
    await load.save();
  } catch (error) {
    throw error;
  }

  res
    .status(200)
    .send({ message: 'Load created successfully' });
};

const getActivLoad = async (req, res) => {
  const { userId } = req.user;

  const truck = await Truck.findOne({ assigned_to: userId });

  if (!truck) {
    res
      .status(400)
      .send({
        message: "You don't have assigned truck",
        driver_found: false,
      });
    return;
  }

  const load = await Load.find({ assigned_to: truck._id, status: 'ASSIGNED' });

  if (!load) {
    res
      .status(400)
      .send({
        message: "You don't have active load",
        driver_found: false,
      });
    return;
  }

  res
    .status(200)
    .send({
      load: load[0],
    });
};

const changeLoadState = async (req, res) => {
  const { userId } = req.user;

  const truck = await Truck.findOneAndUpdate({ assigned_to: userId }, { status: 'IS' });

  if (!truck) {
    res
      .status(400)
      .send({
        message: "You don't have assigned truck",
        driver_found: false,
      });
    return;
  }

  const load = await Load.findOne({ assigned_to: truck._id, status: 'ASSIGNED' });

  if (load.state === 'En route to Pick Up') {
    await load.update({
      state: 'Arrived to Pick Up',
    });

    res
      .status(200)
      .send({
        message: "Load state changed to 'Arrived to Pick Up'",
      });
    return;
  } if (load.state === 'Arrived to Pick Up') {
    await load.updateOne({
      state: 'En route to delivery',
    });

    res
      .status(200)
      .send({
        message: "Load state changed to 'En route to delivery'",
      });
    return;
  } if (load.state === 'En route to delivery') {
    await load.updateOne({
      state: 'Arrived to delivery',
      status: 'SHIPPED',
    });

    await truck.updateOne({
      assigned_to: null,
    });
    res
      .status(200)
      .send({
        message: "Load state changed to 'Arrived to delivery'",
      });
    return;
  }

  if (!load) {
    res
      .status(400)
      .send({
        message: "You don't have active load",
      });
  }
};

const getLoadById = async (req, res) => {
  const { id } = req.params;

  const load = await Load.findById({ _id: id });

  res
    .status(200)
    .send({
      load,
    });
};

const updateLoad = async (req, res) => {
  const { id } = req.params;

  const load = await Load.findById({ _id: id });

  const {
    name = load.name,
    payload = load.payload,
    pickup_address = load.pickup_address,
    delivery_address = load.delivery_address,
    dimensions = load.dimensions,
    status = load.status,
  } = req.body;

  await LoadJoiSchema.validateAsync({
    name, payload, pickup_address, delivery_address, dimensions, status,
  });

  await load.updateOne({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  res
    .status(200)
    .send({ message: 'Load details changed successfully' });
};

const deleteLoad = async (req, res) => {
  const { id } = req.params;

  const load = await Load.findById({ _id: id });
  load.delete();

  res
    .status(200)
    .send({
      message: 'Load deleted successfully!',
    });
};

const searchDriver = async (req, res) => {
  const { id } = req.params;
  const load = await Load.findById({ _id: id });
  const { width, length, height } = load.dimensions;
  const { payload } = load;

  const truck = await findTruck(width, length, height, payload);

  if (!truck) {
    res
      .status(400)
      .send({
        message: 'Load posted fail',
        driver_found: false,
      });
    return;
  }

  await load.updateOne({
    status: 'ASSIGNED',
    assigned_to: truck._id,
    state: 'En route to Pick Up',
    logs: [{
      message: `Load assigned to driver with id ${truck.assigned_to}`,
      time: new Types.ObjectId().getTimestamp().toISOString(),
    }],
  });
  res
    .status(200)
    .send({
      message: 'Load posted successfully',
      driver_found: true,
    });
};

const getLoadShippingInfo = async (req, res) => {
  const { id } = req.params;

  const load = await Load.findById({ _id: id });

  const truck = await Truck.findById({ _id: load.assigned_to });

  res
    .status(200)
    .send({
      load,
      truck,
    });
};

module.exports = {
  getUsersLoads,
  addLoad,
  getActivLoad,
  changeLoadState,
  getLoadById,
  updateLoad,
  deleteLoad,
  searchDriver,
  getLoadShippingInfo,
};
