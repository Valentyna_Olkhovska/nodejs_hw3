const bcrypt = require('bcryptjs');
const { User, AvatarJoiSchema } = require('../models/Users.js');

const userInfo = async (req, res) => {
  const { userId } = req.user;

  const user = await User.findById(userId);

  res
    .status(200)
    .send({
      user: user.toJSON(),
    });
};

const changePassword = async (req, res) => {
  const { userId } = req.user;
  const { newPassword, oldPassword } = req.body;

  const user = await User.findById(userId);

  const isSamePassword = oldPassword === newPassword;

  if (isSamePassword) {
    res
      .status(400)
      .send({
        message: 'New password can\'t be the same as the previous one',
      });

    return;
  }

  const isPasswordConfirmed = await bcrypt.compare(oldPassword, user.password);

  if (!isPasswordConfirmed) {
    res
      .status(400)
      .send({ message: 'Password is invalid' });

    return;
  }

  await user.updateOne({
    password: await bcrypt.hash(newPassword, 10),
  });

  await user.save();

  res
    .status(200)
    .send({ message: 'Password changed successfully' });
};

const deleteUser = async (req, res) => {
  const { userId } = req.user;

  const user = await User.findById(userId);

  user.delete();

  res
    .status(200)
    .send({
      message: 'Deleted successfully!',
    });
};

const uploadAvatar = async (req, res) => {
  const { userId } = req.user;

  const user = await User.findById(userId);

  await AvatarJoiSchema.validateAsync(req.file.mimetype);

  const avatar = req.file.buffer.toString('base64');

  await user.updateOne({
    avatar,
  });

  res.status(200).send({
    message: 'Success',
    avatar,
  });
};

module.exports = {
  userInfo,
  changePassword,
  deleteUser,
  uploadAvatar,
};
