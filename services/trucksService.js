const bcrypt = require('bcryptjs');
const { Types } = require('mongoose');
const { Truck, TruckJoiSchema } = require('../models/Truck.js');

const getUsersTrucks = async (req, res) => {
  const { userId } = req.user;

  const trucks = await Truck.find({ created_by: userId });

  res
    .status(200)
    .send({
      trucks,
    });
};

const addTrack = async (req, res) => {
  const { userId } = req.user;
  const { type } = req.body;

  await TruckJoiSchema.validateAsync({ type });

  const truck = new Truck({
    type,
    status: 'IS',
    created_by: userId,
    createdDate: new Types.ObjectId().getTimestamp().toISOString(),

  });

  try {
    await truck.save();
  } catch (error) {
    throw error;
  }

  res
    .status(200)
    .send({ message: 'Truck created successfully' });
};

const getUsersTrackById = async (req, res) => {
  const { id } = req.params;

  const truck = await Truck.findById({ _id: id });

  res
    .status(200)
    .send({
      truck,
    });
};

const updateTrack = async (req, res) => {
  const { id } = req.params;
  const { type } = req.body;

  const truck = await Truck.findById({ _id: id });

  await TruckJoiSchema.validateAsync({ type });

  await truck.updateOne({
    type,
  });

  res
    .status(200)
    .send({ message: 'Truck update successfully' });
};

const deleteTruck = async (req, res) => {
  const { id } = req.params;

  const truck = await Truck.findById({ _id: id });
  truck.delete();

  res
    .status(200)
    .send({
      message: 'Truck deleted successfully!',
    });
};

const assignTruck = async (req, res) => {
  const { id } = req.params;
  const { userId } = req.user;
  const status = 'IS';
  const truck = await Truck.findById({ _id: id });

  await TruckJoiSchema.validateAsync({ status });

  await truck.updateOne({
    assigned_to: userId,
    status,
  });
  res
    .status(200)
    .send({ message: 'Truck assigned successfully' });
};

module.exports = {
  getUsersTrucks,
  addTrack,
  getUsersTrackById,
  updateTrack,
  deleteTruck,
  assignTruck,
};
