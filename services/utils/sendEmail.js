const nodemailer = require('nodemailer');

const sendEmail = async (options) => {
  const transporeter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.MY_EMAIL,
      pass: process.env.MY_PASSWORD,
    },
  });

  console.log(options.message);

  const message = {
    from: `${process.env.MY_EMAIL}`,
    to: options.email,
    subject: options.subject,
    html: options.message,
  };

  await transporeter.sendMail(message);
};

module.exports = sendEmail;
