const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Types } = require('mongoose');
const fs = require('fs');
const path = require('path');

const { User, UserJoiSchema } = require('../models/Users.js');
const sendEmail = require('./utils/sendEmail.js');

const MongoErrors = {
  DUPLICATE_CODE: 11000,
};

const registerUser = async (req, res) => {
  const { password, role, email } = req.body;
  await UserJoiSchema.validateAsync({ password, role, email });

  const user = new User({
    password: await bcrypt.hash(password, 10),
    role,
    email,
    createdDate: new Types.ObjectId().getTimestamp().toISOString(),
  });

  try {
    await user.save();
  } catch (error) {
    if (error.code === MongoErrors.DUPLICATE_CODE) {
      res
        .status(400)
        .send({ message: 'User already exist!' });

      return;
    }
    throw error;
  }

  res
    .status(200)
    .send({ message: 'Success' });
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    res
      .status(400)
      .json({ message: 'Username or password is invalid' });
    return;
  }

  const isPasswordCorrect = await bcrypt.compare(password, user.password);

  if (!isPasswordCorrect) {
    res
      .status(400)
      .json({ message: 'Username or password is invalid' });

    return;
  }

  const payload = { email: user.email, userId: user._id };
  const jwtToken = jwt.sign(payload, process.env.JWT_KEY);

  return res
    .status(200)
    .send({
      message: 'Success',
      jwt_token: jwtToken,
    });
};

const forgotPassword = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    res.status(404).send({ message: 'There is no user with that email' });
    return;
  }

  const payload = { email: user.email, key: process.env.RESET };
  const resetToken = jwt.sign(payload, process.env.JWT_KEY, { expiresIn: '10m' });

  const resetUrl = `http://localhost:3000/resetPassword.html?token=${resetToken}`;

  try {
    await sendEmail({
      email: user.email,
      subject: 'Password reset token',
      message: fs.readFileSync(path.resolve(__dirname, '../Email/email.html'))
        .toString()
        .replace('LINK_PLACEHOLDER', resetUrl),
    });
    res.status(200).send({ message: 'Email send succses' });
  } catch (error) {
    console.log(error.message);
  }
};

const resetPassword = async (req, res) => {
  const { token } = req.params;

  console.log(token);
  const payload = jwt.verify(token, process.env.JWT_KEY);

  const user = await User.findOne({ email: payload.email });

  console.log(user);

  if (!user) {
    res.status(404).send({ message: 'User not found' });
    return;
  }
  const { password } = req.body;

  user.password = await bcrypt.hash(password, 10);
  await user.save();

  res.status(200).send({ message: 'Resset password succes!' });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
  resetPassword,
};
