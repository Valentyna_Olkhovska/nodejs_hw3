const { User } = require('../models/Users.js');

const shipperMiddleware = async (req, res, next) => {
  const { userId } = req.user;

  const user = await User.findById(userId);

  const { role } = user;

  try {
    if (role !== 'SHIPPER') {
      res
        .status(400)
        .send({
          message: 'This option is available only for shipper',
        });

      return;
    }

    next();
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

module.exports = {
  shipperMiddleware,
};
