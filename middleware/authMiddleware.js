const jwt = require('jsonwebtoken');
const { User } = require('../models/Users');

require('dotenv').config();

const authMiddleware = (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res
      .status(400)
      .json({
        message: 'Please, provide authorization header',
      });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res
      .status(400)
      .json({
        message: 'Please, include token to request',
      });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_KEY);

    req.user = {
      userId: tokenPayload.userId,
      email: tokenPayload.email,
    };

    const user = User.findById(req.user.userId);

    if (!user) {
      res
        .status(404)
        .send({
          message: 'User not found',
        });

      return;
    }

    next();
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

module.exports = {
  authMiddleware,
};
