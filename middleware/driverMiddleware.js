const { User } = require('../models/Users.js');

const driverMiddleware = async (req, res, next) => {
  const { userId } = req.user;

  const user = await User.findById(userId);

  const { role } = user;

  try {
    if (role !== 'DRIVER') {
      res
        .status(400)
        .send({
          message: 'This option is available only for driver',
        });

      return;
    }

    next();
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

module.exports = {
  driverMiddleware,
};
