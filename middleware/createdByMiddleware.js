const { Truck } = require('../models/Truck');
const { Load } = require('../models/Load');

const createdByMiddleware = async (req, res, next) => {
  const { userId } = req.user;
  const { id } = req.params;

  if (id.length !== 24) {
    res
      .status(400)
      .send({
        message: 'Not found!',
      });

    return;
  }

  const truck = await Truck.findById({ _id: id });
  const load = await Load.findById({ _id: id });

  try {
    if (!truck && !load) {
      res
        .status(400)
        .send({
          message: ' Not found',
        });

      return;
    }

    if ((truck && truck.created_by !== userId || (load && load.created_by !== userId))) {
      res
        .status(400)
        .send({
          message: 'Item not found',
        });

      return;
    }

    next();
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

module.exports = {
  createdByMiddleware,
};
