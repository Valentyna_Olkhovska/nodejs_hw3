document.addEventListener('DOMContentLoaded', () => {
  const submitButton = document.querySelector('#submit-button');

  submitButton.addEventListener('click', async () => {
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;
    const role = document.querySelector('#role').value;

    try {
      await axios.post('http://localhost:8080/api/auth/register', {
        email,
        password,
        role,
      });

      location.href = './login.html';
    } catch (error) {
      console.log(error);
      alert(error.response.data.message);
    }
  });
});
