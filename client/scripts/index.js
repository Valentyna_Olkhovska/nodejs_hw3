document.addEventListener('DOMContentLoaded', () => {
  const isAuthorized = !!localStorage.getItem('JWT');

  async function getRole(token) {
    const response = await axios.get('http://localhost:8080/api/users/me', {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });

    return response.data.user.role;
  }

  const role = getRole(localStorage.getItem('JWT'));

  if (isAuthorized && role === 'DRIVER') {
    location.href = './driver/profileDriver.html';
  } else if (isAuthorized && role === 'SHIPPER') {
    location.href = './shipper/profileShipper.html';
  } else {
    location.href = './login.html';
  }
});
