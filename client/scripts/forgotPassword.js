document.addEventListener('DOMContentLoaded', () => {
  const submitButton = document.querySelector('#submit-button');

  submitButton.addEventListener('click', async () => {
    const email = document.querySelector('#email').value;

    try {
      const response = await axios.post('http://localhost:8080/api/auth/forgot_password', {
        email,
      });
      alert('Send email');
    } catch (error) {
      alert(error.response.data.message);
    }
  });
});
