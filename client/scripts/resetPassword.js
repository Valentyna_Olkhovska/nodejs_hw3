document.addEventListener('DOMContentLoaded', () => {
  const submitButton = document.querySelector('#submit-button');

  const params = new URLSearchParams(window.location.search);

  const token = params.get('token');

  submitButton.addEventListener('click', async () => {
    const password = document.querySelector('#password').value;

    try {
      const response = await axios.put(`http://localhost:8080/api/auth/reset_password/${token}`, {
        password,
      });
      alert('Resset password succes!');
      location.href = './login.html';
    } catch (error) {
      alert(error.response.data.message);
    }
  });
});
