document.addEventListener('DOMContentLoaded', () => {
  const submitButton = document.querySelector('#submit-button');

  async function getRole(token) {
    const response = await axios.get('http://localhost:8080/api/users/me', {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });

    return response.data.user.role;
  }

  submitButton.addEventListener('click', async () => {
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;

    try {
      const { data } = await axios.post('http://localhost:8080/api/auth/login', {
        email,
        password,
      });

      localStorage.setItem('JWT', data.jwt_token);

      const role = await getRole(data.jwt_token);

      if (role === 'DRIVER') {
        location.href = '/driver/profileDriver.html';
        return;
      } if (role === 'SHIPPER') {
        location.href = '/shipper/profileShipper.html';
        return;
      }
      return error;
    } catch (error) {
      alert(error.response.data.message);
    }
  });
});
