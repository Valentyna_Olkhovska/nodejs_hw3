document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '../login.html';
    return;
  }

  const loadContainer = document.querySelector('#load-container');
  const submitButton = document.querySelector('.submit-button');
  const loadIdInput = document.querySelector('#loadId');

  const signOutButton = document.querySelector('#sign-out');
  const createButton = document.querySelector('#create');
  const profileButton = document.querySelector('#profile');
  const loadListButton = document.querySelector('#loadList');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createLoad.html';
  });

  profileButton.addEventListener('click', async () => {
    location.href = './profileShipper.html';
  });
  loadListButton.addEventListener('click', async () => {
    location.href = './loadList.html';
  });

  submitButton.addEventListener('click', async () => {
    console.log(loadIdInput.value);
    const loadId = loadIdInput.value;

    try {
      const response = await axios.get(`http://localhost:8080/api/loads/${loadId}/shipping_info`, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });

      loadContainer.innerHTML = '';

      const idItem = document.createElement('div');
      const nameItem = document.createElement('div');
      const statusItem = document.createElement('div');
      const stateItem = document.createElement('div');
      const pickupAddressItem = document.createElement('div');
      const deliveryAddressItem = document.createElement('div');
      const payloadItem = document.createElement('div');
      const widthItem = document.createElement('div');
      const lengthItem = document.createElement('div');
      const heightItem = document.createElement('div');
      const messageItem = document.createElement('div');
      const assignedToItem = document.createElement('div');
      const dateItem = document.createElement('div');
      const truckItem = document.createElement('div');
      const truckIdItem = document.createElement('div');
      const createdTruckItem = document.createElement('div');
      const assignedTruckItem = document.createElement('div');
      const typeTruckItem = document.createElement('div');
      const statusTruckItem = document.createElement('div');
      const dateTruckItem = document.createElement('div');

      const { load } = response.data;
      const { truck } = response.data;

      const content = document.createElement('span');

      const [logs] = load.logs;

      idItem.innerHTML = `<p>Id: </p> <p>${load._id}</p>`;
      nameItem.innerHTML = `<p>Name: </p> <p>${load.name}</p>`;
      statusItem.innerHTML = `<p>Status:</p> <p>${load.status}</p>`;
      stateItem.innerHTML = `<p>State:</p> <p>${load.state}</p>`;
      pickupAddressItem.innerHTML = `<p>Pickup address:</p> <p>${load.pickup_address}</p>`;
      deliveryAddressItem.innerHTML = `<p>Delivery address:</p> <p>${load.delivery_address}</p>`;
      payloadItem.innerHTML = `<p>Payload:</p> <p>${load.payload};</p>`;
      widthItem.innerHTML = `<p>width: </p> <p>${load.dimensions.width};</p>`;
      lengthItem.innerHTML = `<p>length:</p> <p>${load.dimensions.length}</p>`;
      heightItem.innerHTML = `<p>height:</p> <p>${load.dimensions.height};</p>`;
      messageItem.innerHTML = `<p>message: </p> <p>"${logs ? logs.message : '-'}"</p>`;
      assignedToItem.innerHTML = `<p>Assigned to:</p> <p> ${load.assigned_to};</p>`;
      dateItem.innerHTML = `<p>Created date: </p> <p>${load.createdDate};</p>`;
      truckItem.innerHTML = '<p>Truck:</p> ';
      truckIdItem.innerHTML = `<p>Id: </p> <p>${truck._id};</p>`;
      createdTruckItem.innerHTML = `<p>Created by: </p> <p>${truck.created_by}</p>`;
      assignedTruckItem.innerHTML = `<p>Assigned to: </p> <p>${truck.assigned_to}</p>`;
      typeTruckItem.innerHTML = `<p>Type: </p> <p>${truck.type};</p>`;
      statusTruckItem.innerHTML = `<p>Status: </p> <p>${truck.status};</p>`;
      dateTruckItem.innerHTML = `<p>Created Date:</p> <p>${truck.createdDate};</p>`;

      idItem.classList.add('shippingInfoItem');
      nameItem.classList.add('shippingInfoItem');
      statusItem.classList.add('shippingInfoItem');
      stateItem.classList.add('shippingInfoItem');
      pickupAddressItem.classList.add('shippingInfoItem');
      deliveryAddressItem.classList.add('shippingInfoItem');
      payloadItem.classList.add('shippingInfoItem');
      widthItem.classList.add('shippingInfoItem');
      lengthItem.classList.add('shippingInfoItem');
      heightItem.classList.add('shippingInfoItem');
      messageItem.classList.add('shippingInfoItem');
      assignedToItem.classList.add('shippingInfoItem');
      dateItem.classList.add('shippingInfoItem');
      truckItem.classList.add('shippingInfoItem');
      truckIdItem.classList.add('shippingInfoItem');
      createdTruckItem.classList.add('shippingInfoItem');
      assignedTruckItem.classList.add('shippingInfoItem');
      typeTruckItem.classList.add('shippingInfoItem');
      statusTruckItem.classList.add('shippingInfoItem');
      dateTruckItem.classList.add('shippingInfoItem');

      loadContainer.appendChild(idItem);
      loadContainer.appendChild(nameItem);
      loadContainer.appendChild(statusItem);
      loadContainer.appendChild(stateItem);
      loadContainer.appendChild(pickupAddressItem);
      loadContainer.appendChild(deliveryAddressItem);
      loadContainer.appendChild(payloadItem);
      loadContainer.appendChild(widthItem);
      loadContainer.appendChild(lengthItem);
      loadContainer.appendChild(heightItem);
      loadContainer.appendChild(messageItem);
      loadContainer.appendChild(assignedToItem);
      loadContainer.appendChild(dateItem);
      loadContainer.appendChild(truckItem);
      loadContainer.appendChild(truckIdItem);
      loadContainer.appendChild(createdTruckItem);
      loadContainer.appendChild(assignedTruckItem);
      loadContainer.appendChild(typeTruckItem);
      loadContainer.appendChild(statusTruckItem);
      loadContainer.appendChild(dateTruckItem);
    } catch (error) {
      alert(error.response.data.message);
    }
  });
});
