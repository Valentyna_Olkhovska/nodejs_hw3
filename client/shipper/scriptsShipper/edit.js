document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '/login.html';
    return;
  }

  const signOutButton = document.querySelector('#sign-out');
  const saveButton = document.querySelector('#save');
  const cancelButton = document.querySelector('#cancel');
  const createButton = document.querySelector('#create');
  const profileButton = document.querySelector('#profile');
  const loadListButton = document.querySelector('#loadList');

  const nameInput = document.querySelector('#loadName');
  const payloadInput = document.querySelector('#payload');
  const picupAddressInput = document.querySelector('#loadPickupAddress');
  const deliveryAddressInput = document.querySelector('#loadDeliveryAddress');
  const widthInput = document.querySelector('#loadDimentsionsWidth');
  const lenghtInput = document.querySelector('#loadDimentsionsLength');
  const heightInput = document.querySelector('#loadDimentsionsHeight');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '/login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createLoad.html';
  });

  profileButton.addEventListener('click', async () => {
    location.href = './profileShipper.html';
  });
  loadListButton.addEventListener('click', async () => {
    location.href = './loadList.html';
  });

  const searchParams = new URLSearchParams(location.search);
  const loadId = searchParams.get('id');

  try {
    const response = await axios.get(`http://localhost:8080/api/loads/${loadId}`, {
      headers: {
        authorization: `Bearer ${JWT}`,
      },
    });
    const { load } = response.data;

    nameInput.value = load.name;
    payloadInput.value = load.payload;
    picupAddressInput.value = load.pickup_address;
    deliveryAddressInput.value = load.delivery_address;
    widthInput.value = load.dimensions.width;
    lenghtInput.value = load.dimensions.length;
    heightInput.value = load.dimensions.height;
  } catch (error) {
    alert(error.response.data.message);
  }

  saveButton.addEventListener('click', async () => {
    try {
      await axios.put(`http://localhost:8080/api/loads/${loadId}`, {
        name: nameInput.value,
        payload: payloadInput.value,
        pickup_address: picupAddressInput.value,
        delivery_address: deliveryAddressInput.value,
        dimensions: {
          width: widthInput.value,
          length: lenghtInput.value,
          height: heightInput.value,
        },
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });
      location.href = `/shipper/load.html?id=${loadId}`;
    } catch (error) {
      alert(error.response.data.message);
    }
  });

  cancelButton.addEventListener('click', async () => {
    location.href = `/shipper/load.html?id=${loadId}`;
  });
});
