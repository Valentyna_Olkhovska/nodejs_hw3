document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '/login.html';
    return;
  }

  const saveButton = document.querySelector('#save-button');
  const signOutButton = document.querySelector('#sign-out');

  const nameInput = document.querySelector('#loadName');
  const payloadInput = document.querySelector('#payload');
  const picupAddressInput = document.querySelector('#loadPickupAddress');
  const deliveryAddressInput = document.querySelector('#loadDeliveryAddress');
  const widthInput = document.querySelector('#loadDimentsionsWidth');
  const lenghtInput = document.querySelector('#loadDimentsionsLength');
  const heightInput = document.querySelector('#loadDimentsionsHeight');

  saveButton.addEventListener('click', async () => {
    try {
      await axios.post('http://localhost:8080/api/loads/', {
        name: nameInput.value,
        payload: payloadInput.value,
        pickup_address: picupAddressInput.value,
        delivery_address: deliveryAddressInput.value,
        dimensions: {
          width: widthInput.value,
          length: lenghtInput.value,
          height: heightInput.value,
        },
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });
      location.href = '/shipper/loadList.html';
    } catch (error) {
      alert(error.response.data.message);
    }
  });

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });
});
