document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '../login.html';
    return;
  }

  const loadContainer = document.querySelector('.load-container');
  const deleteButton = document.querySelector('.delete-button');
  const editButton = document.querySelector('.edit-button');
  const searchButton = document.querySelector('.search-drivers-button');

  const signOutButton = document.querySelector('#sign-out');
  const createButton = document.querySelector('#create');
  const profileButton = document.querySelector('#profile');
  const loadListButton = document.querySelector('#loadList');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createLoad.html';
  });

  profileButton.addEventListener('click', async () => {
    location.href = './profileShipper.html';
  });
  loadListButton.addEventListener('click', async () => {
    location.href = './loadList.html';
  });

  const searchParams = new URLSearchParams(location.search);
  const loadId = searchParams.get('id');

  try {
    const response = await axios.get(`http://localhost:8080/api/loads/${loadId}`, {
      headers: {
        authorization: `Bearer ${JWT}`,
      },
    });

    loadContainer.innerHTML = '';

    const { load } = response.data;

    const [logs] = load.logs;

    loadContainer.innerHTML = `

        <div class="load-item  list-item"> <p>Id:</p> <p>${load._id};</p></div>
        <div class="load-item list-item"><p> Name: </p> <p>${load.name}</p></div>
        <div class="load-item list-item"><p> Status: </p> <p>${load.status}</p></div>
        <div class="load-item list-item"><p> State: </p> <p>${load.state}</p></div>
        <div class="load-item list-item"><p>Pickup address: </p> <p>${load.pickup_address}</p></div>
        <div class="load-item list-item"><p> Delivery address: </p> <p>${load.delivery_address}</p></div>
        <div class="load-item list-item"><p>Width:</p> <p>${load.dimensions.width}</p></div>
        <div class="load-item list-item"><p>Length:</p> <p>${load.dimensions.length}</p></div>
        <div class="load-item list-item"><p>Height:</p> <p>${load.dimensions.height}</p></div>
        <div class="load-item list-item"><p>Log message:</p> <p>${logs ? logs.message : '-'}</p></div>
        <div class="load-item list-item"><p>Message time:</p> <p>${logs ? logs.time : '-'}</p></div>
        <div class="load-item list-item"><p>Created date:</p> <p>${load.createdDate}</p></div>
         `;
    editButton.addEventListener('click', async () => {
      location.href = `/shipper/edit.html?id=${loadId}`;
    });

    deleteButton.addEventListener('click', async () => {
      const confirmed = confirm('Are you sure?');

      if (!confirmed) {
        return;
      }

      try {
        await axios.delete(`http://localhost:8080/api/loads/${loadId}`, {
          headers: {
            authorization: `Bearer ${JWT}`,
          },
        });
        location.href = '/shipper/loadList.html';
      } catch (error) {
        alert(error.response.data.message);
      }
    });

    searchButton.addEventListener('click', async () => {
      try {
        await axios.post(`http://localhost:8080/api/loads/${loadId}/post`, {

        }, {
          headers: {
            authorization: `Bearer ${JWT}`,
          },
        });

        alert('Load posted successfully!Driver was found!');
        location.href = '/shipper/loadList.html';
      } catch (error) {
        console.log(error);
        alert(error.response.data.message);
      }
    });
  } catch (error) {
    alert(error.response.data.message);
  }
});
