const handleError = (error) => {
  console.log(error);
  alert(error.response?.data?.message || 'unkown error');
};

const getBase64Source = (base64) => `data:image/png;base64,${base64}`;

document.addEventListener('DOMContentLoaded', async () => {
  const signOutButton = document.querySelector('#sign-out');
  const createButton = document.querySelector('#create');
  const shippingInfoButton = document.querySelector('#shippingInfo');
  const changePassButton = document.querySelector('#change-pas');
  const deleteAccButton = document.querySelector('#delete-acc');
  const changePassForm = document.querySelector('#change-password-form');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createLoad.html';
  });

  shippingInfoButton.addEventListener('click', async () => {
    location.href = './shippingInfo.html';
  });

  const avatar = document.querySelector('#avatar');
  const avatarInput = document.querySelector('#avatar-input');

  const token = localStorage.getItem('JWT');

  try {
    const response = await axios.get('http://localhost:8080/api/users/me', {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });

    avatar.src = response.data.user.avatar ? getBase64Source(response.data.user.avatar) : '../image/avatar.png';
  } catch (error) {
    handleError(error);
  }

  avatarInput.addEventListener('change', async (event) => {
    try {
      const [file] = event.currentTarget.files;
      const formData = new FormData();

      formData.append('avatar', file);

      const response = await axios.patch('http://localhost:8080/api/users/me/avatar', formData, {
        headers: {
          authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
      });

      avatar.src = getBase64Source(response.data.avatar);
    } catch (error) {
      handleError(error);
    }
  });

  changePassButton.addEventListener('click', async () => {
    changePassForm.classList.remove('hidden');
    window.scrollTo({
      top: document.body.scrollHeight,
      behavior: 'smooth',
    });

    const oldPasswordInput = document.querySelector('#oldPass');
    const newPasswordInput = document.querySelector('#newPass');
    const saveButton = document.querySelector('#submit-button');

    saveButton.addEventListener('click', async () => {
      try {
        const response = await axios.patch('http://localhost:8080/api/users/me/password', {

          oldPassword: oldPasswordInput.value,
          newPassword: newPasswordInput.value,

        }, {
          headers: {
            authorization: `Bearer ${token}`,

          },
        });
        alert('Your password has been changed');
      } catch (error) {
        alert(error.response.data.message);
      }
    });
  });

  deleteAccButton.addEventListener('click', async () => {
    try {
      const response = await axios.delete('http://localhost:8080/api/users/me', {
        headers: {
          authorization: `Bearer ${token}`,

        },
      });
    } catch (error) {
      handleError(error);
    }
  });
});
