document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '/login.html';
    return;
  }

  const sprinterContainer = document.querySelector('#sprinter');
  const smallStraightContainer = document.querySelector('#small-straight');
  const largeStraightContainer = document.querySelector('#large-straight');

  const signOutButton = document.querySelector('#sign-out');

  const truckContainer = document.querySelector('.truck-container');

  sprinterContainer.addEventListener('click', async () => {
    try {
      await axios.post('http://localhost:8080/api/trucks/', {
        type: 'SPRINTER',
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });

      alert('Truck type: Sprinter created successfully!');

    } catch (error) {
      alert(error.response.data.message);
    }
  });
  smallStraightContainer.addEventListener('click', async () => {
    try {
      await axios.post('http://localhost:8080/api/trucks/', {
        type: 'SMALL STRAIGHT',
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });

      alert('Truck type: Small Straight created successfully!');

    } catch (error) {
      alert(error.response.data.message);
    }
  });
  largeStraightContainer.addEventListener('click', async () => {
    try {
      await axios.post('http://localhost:8080/api/trucks/', {
        type: 'LARGE STRAIGHT',
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });

      alert('Truck type: Large Straight created successfully!');

    } catch (error) {
      alert(error.response.data.message);
    }
  });

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });
});
