document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '/login.html';
    return;
  }

  const sprinterContainer = document.querySelector('#sprinter');
  const smallStraightContainer = document.querySelector('#small-straight');
  const largeStraightContainer = document.querySelector('#large-straight');

  const signOutButton = document.querySelector('#sign-out');

  const searchParams = new URLSearchParams(location.search);
  const truckId = searchParams.get('id');

  async function getCurentTruckType() {
    try {
      const response = await axios.get(`http://localhost:8080/api/trucks/${truckId}`, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });
      const { truck } = response.data;

      return truck.type;
    } catch (error) {
      alert(error.response.data.message);
    }
  }

  const type = await getCurentTruckType();

  if (type === 'SPRINTER') {
    sprinterContainer.classList.add('current-truck');
  }
  if (type === 'SMALL STRAIGHT') {
    smallStraightContainer.classList.add('current-truck');
  }
  if (type === 'LARGE STRAIGHT') {
    largeStraightContainer.classList.add('current-truck');
  }

  sprinterContainer.addEventListener('click', async () => {
    try {
      await axios.put(`http://localhost:8080/api/trucks/${truckId}`, {
        type: 'SPRINTER',
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });

      smallStraightContainer.classList.remove('current-truck');
      largeStraightContainer.classList.remove('current-truck');
      sprinterContainer.classList.add('current-truck');

      alert('Truck type edit successfully!');
    } catch (error) {
      alert(error.response.data.message);
    }
  });
  smallStraightContainer.addEventListener('click', async () => {
    try {
      await axios.put(`http://localhost:8080/api/trucks/${truckId}`, {
        type: 'SMALL STRAIGHT',
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });
      sprinterContainer.classList.remove('current-truck');
      largeStraightContainer.classList.remove('current-truck');

      smallStraightContainer.classList.add('current-truck');

      alert('Truck type edit successfully!');
    } catch (error) {
      alert(error.response.data.message);
    }
  });
  largeStraightContainer.addEventListener('click', async () => {
    try {
      await axios.put(`http://localhost:8080/api/trucks/${truckId}`, {
        type: 'LARGE STRAIGHT',
      }, {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
      });

      sprinterContainer.classList.remove('current-truck');
      smallStraightContainer.classList.remove('current-truck');
      largeStraightContainer.classList.add('current-truck');

      alert('Truck type edit successfully!');
    } catch (error) {
      alert(error.response.data.message);
    }
  });

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });
});
