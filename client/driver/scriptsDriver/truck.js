document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '../login.html';
    return;
  }

  const truckContainer = document.querySelector('.truck-container');
  const deleteButton = document.querySelector('.delete-button');
  const editButton = document.querySelector('.edit-button');
  const assignButton = document.querySelector('.assign-drivers-button');

  const signOutButton = document.querySelector('#sign-out');
  const createButton = document.querySelector('#create');
  const profileButton = document.querySelector('#profile');
  const truckListButton = document.querySelector('#truckList');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createTruck.html';
  });

  profileButton.addEventListener('click', async () => {
    location.href = './profileShipper.html';
  });
  truckListButton.addEventListener('click', async () => {
    location.href = './truckList.html';
  });

  function getInfo(truck) {
    if (truck.type === 'SPRINTER') {
      return {
        image: 'sprinter.png', payload: 1700, width: 300, height: 250, length: 170,
      };
    }
    if (truck.type === 'SMALL STRAIGHT') {
      return {
        image: 'straightS.png', payload: 2500, width: 500, height: 250, length: 170,
      };
    }
    if (truck.type === 'LARGE STRAIGHT') {
      return {
        image: 'straightL.png', payload: 4000, width: 700, height: 350, length: 200,
      };
    }
  }

  const searchParams = new URLSearchParams(location.search);
  const truckId = searchParams.get('id');

  try {
    const response = await axios.get(`http://localhost:8080/api/trucks/${truckId}`, {
      headers: {
        authorization: `Bearer ${JWT}`,
      },
    });

    truckContainer.innerHTML = '';

    const truckImage = document.createElement('div');
    const truckContent = document.createElement('div');

    truckImage.classList.add('truck-image-container');
    truckContent.classList.add('item-content');

    const { truck } = response.data;

    const typeOptions = getInfo(truck);
    const {
      image, payload, width, height, length,
    } = typeOptions;

    truckImage.innerHTML = `
                <img class="truck-image" src="../image/${image}" alt="truck image">
                `;

    truckContent.innerHTML = `
        <div class="list-item"><p> Id: </p> <p>${truck._id}</p></div>
        <div class="list-item"><p>Created by: </p> <p>${truck.created_by}</p></div>
        <div class="list-item"><p>Assigned to: </p> <p>${truck.assigned_to ? truck.assigned_to : 'none'}</p></div>
        <div class="list-item"><p>Type:</p><p>${truck.type}</p></div>
        <div class="list-item"><p>Payload:</p><p>${payload}</p></div>
        <div class="list-item"><p>Width for load:</p><p>${width}</p></div>
        <div class="list-item"><p>Height for load:</p><p>${height}</p></div>
        <div class="list-item"><p>Length:</p><p>${length}</p></div>
        <div class="list-item"> <p>Status: </p> <p>${truck.status}</p></div>
        <div class="list-item"><p>Created date: </p><p>${truck.createdDate}</p></div>
        `;
    truckContainer.appendChild(truckImage);
    truckContainer.appendChild(truckContent);

    editButton.addEventListener('click', async () => {
      location.href = `/driver/editTruck.html?id=${truckId}`;
    });

    deleteButton.addEventListener('click', async () => {
      const confirmed = confirm('Are you sure?');

      if (!confirmed) {
        return;
      }

      try {
        await axios.delete(`http://localhost:8080/api/trucks/${truckId}`, {
          headers: {
            authorization: `Bearer ${JWT}`,
          },
        });
        location.href = '/driver/truckList.html';
      } catch (error) {
        alert(error.response.data.message);
      }
    });

    assignButton.addEventListener('click', async () => {
      alert('You are assigned on this truck!');
      try {
        await axios.post(`http://localhost:8080/api/trucks/${truckId}/assign`, {

        }, {
          headers: {
            authorization: `Bearer ${JWT}`,
          },
        });

        location.href = `/driver/truck.html?id=${truck._id}`;
      } catch (error) {
        console.log(error);
        alert(error.response.data.message);
      }
    });
  } catch (error) {
    alert(error.response.data.message);
  }
});
 