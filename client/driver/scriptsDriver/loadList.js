document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '/login.html';
    return;
  }

  const signOutButton = document.querySelector('#sign-out');
  const createButton = document.querySelector('#create');
  const profileButton = document.querySelector('#profile');

  const loadsContainer = document.querySelector('#loads-container');
  const submitButton = document.querySelector('.submit-button');

  const statusInput = document.querySelector('#status-driver-input');
  const pagination = document.querySelector('#pagination');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createTruck.html';
  });

  profileButton.addEventListener('click', async () => {
    location.href = './profileDriver.html';
  });

  const pageSize = 3;

  const total = await fetchLoads(0);

  const pagesCount = Math.ceil(total / pageSize);

  new Array(pagesCount).fill(undefined).forEach((e, index) => {
    const li = document.createElement('li');
    li.innerHTML = index + 1;

    pagination.appendChild(li);
    if (index === 0) {
      li.classList.add('active');
    }

    li.addEventListener('click', () => {
      const active = document.querySelector('.active');
      if (active) {
        active.classList.remove('active');
      }
      li.classList.add('active');

      fetchLoads(index * pageSize);
    });
  });

  function filterStatus(offset) {
    if (!statusInput.value) {
      return {
        headers: {
          authorization: `Bearer ${JWT}`,
        },
        params: {
          limit: pageSize,
          offset,
        },
      };
    }
    return {
      headers: {
        authorization: `Bearer ${JWT}`,
      },
      params: {
        limit: pageSize,
        offset,
        status: statusInput.value,
      },
    };
  }

  async function fetchLoads(offset) {
    try {
      const response = await axios.get(
        'http://localhost:8080/api/loads',
        filterStatus(offset),
      );

      if (response.data.loads === null) {
        renderLoads(response.data.loads);
      } else {
        renderLoads([response.data.loads]);
      }

      return response.data.count;
    } catch (error) {
      alert(error);
      console.log(error);
      return undefined;
    }
  }

  function renderLoads(loads) {
    loadsContainer.innerHTML = '';

    if (loads === null) {
      return loadsContainer.innerHTML = ' <h5 class="loading-string">Oppps! You have no loads with this parameter...</h5>';
    }

    loads.forEach((load) => {
      const wrapper = document.createElement('div');

      wrapper.classList.add('load-container');

      wrapper.innerHTML = `

       <div class="load-item  list-item"> <p>Id:</p> <p>${load._id};</p></div>
       <div class="load-item list-item"><p> Name: </p> <p>${load.name}</p></div>
       <div class="load-item list-item"><p> Status: </p> <p>${load.status}</p></div>
       <div class="load-item list-item"><p> State: </p> <p>${load.state}</p></div>
       <div class="load-item list-item"><p>Pickup address: </p> <p>${load.pickup_address}</p></div>
       <div class="load-item list-item"><p> Delivery address: </p> <p>${load.delivery_address}</p></div>
       <div class="load-item list-item"><p>Created date:</p> <p>${load.createdDate}</p></div>
        `;
      loadsContainer.appendChild(wrapper);
    });
  }

  submitButton.addEventListener('click', () => {
    fetchLoads(0);
  });
});
