document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '/login.html';
    return;
  }

  const signOutButton = document.querySelector('#sign-out');
  const createButton = document.querySelector('#create');
  const profileButton = document.querySelector('#profile');
  const activLoadButton = document.querySelector('#activ-load');

  const trucksContainer = document.querySelector('#trucks-container');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createTruck.html';
  });

  profileButton.addEventListener('click', async () => {
    location.href = './profileShipper.html';
  });

  activLoadButton.addEventListener('click', async () => {
    location.href = './activLoad.html';
  });

  function getInfo(truck) {
    if (truck.type === 'SPRINTER') {
      return {
        image: 'sprinter.png', payload: 1700, width: 300, height: 250, length: 170,
      };
    }
    if (truck.type === 'SMALL STRAIGHT') {
      return {
        image: 'straightS.png', payload: 2500, width: 500, height: 250, length: 170,
      };
    }
    if (truck.type === 'LARGE STRAIGHT') {
      return {
        image: 'straightL.png', payload: 4000, width: 700, height: 350, length: 200,
      };
    }
  }

  try {
    const response = await axios.get('http://localhost:8080/api/trucks', {
      headers: {
        authorization: `Bearer ${JWT}`,
      },
    });

    if (Object.keys(response.data.trucks).length < 1) {
      return;
    }

    trucksContainer.innerHTML = '';

    response.data.trucks.forEach((truck) => {
      const menuButton = document.createElement('button');
      const menuLink = document.createElement('a');
      const wrapper = document.createElement('div');
      const truckImage = document.createElement('div');
      const truckContent = document.createElement('div');

      menuLink.setAttribute('href', `/driver/truck.html?id=${truck._id}`);
      menuButton.classList.add('options-button');

      wrapper.classList.add('truck-container');
      truckImage.classList.add('truck-image-container');
      truckContent.classList.add('item-content');

      menuButton.innerHTML = 'Options';

      const typeOptions = getInfo(truck);
      const {
        image, payload, width, height, length,
      } = typeOptions;

      truckImage.innerHTML = `
                <img class="truck-image" src="../image/${image}" alt="truck image">
                `;

      truckContent.innerHTML = `
                <div class="list-item"><p> Id: </p> <p>${truck._id}</p></div>
                <div class="list-item"><p>Assigned to: </p> <p>${truck.assigned_to ? truck.assigned_to : 'none'}</p></div>
                <div class="list-item"><p>Type:</p><p>${truck.type}</p></div>
                <div class="list-item"><p>Payload:</p><p>${payload}</p></div>
                <div class="list-item"><p>Width for load:</p><p>${width}</p></div>
                <div class="list-item"><p>Height for load:</p><p>${height}</p></div>
                <div class="list-item"><p>Length:</p><p>${length}</p></div>
                <div class="list-item"> <p>Status: </p> <p>${truck.status}</p></div>
                <div class="list-item"><p>Created date: </p><p>${truck.createdDate}</p></div>
                `;
      wrapper.appendChild(truckImage);
      wrapper.appendChild(truckContent);
      menuLink.appendChild(menuButton);
      wrapper.appendChild(menuLink);

      trucksContainer.appendChild(wrapper);
    });
  } catch (error) {
    alert(error.response.data.message);
  }
});
