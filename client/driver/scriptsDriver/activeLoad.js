document.addEventListener('DOMContentLoaded', async () => {
  const JWT = localStorage.getItem('JWT');

  if (!JWT) {
    location.href = '../login.html';
    return;
  }

  const loadContainer = document.querySelector('#activ-load-container');
  const ChangeStateButton = document.querySelector('#change-state');

  const signOutButton = document.querySelector('#sign-out');
  const createButton = document.querySelector('#create');
  const profileButton = document.querySelector('#profile');
  const truckListButton = document.querySelector('#truckList');

  signOutButton.addEventListener('click', async () => {
    localStorage.removeItem('JWT');

    location.href = '../login.html';
  });

  createButton.addEventListener('click', async () => {
    location.href = './createTruck.html';
  });

  profileButton.addEventListener('click', async () => {
    location.href = './profileShipper.html';
  });
  truckListButton.addEventListener('click', async () => {
    location.href = './truckList.html';
  });

  try {
    const response = await axios.get('http://localhost:8080/api/loads/active', {
      headers: {
        authorization: `Bearer ${JWT}`,
      },
    });

    if (!response.data.load) {
      alert('Ooops...you are have not active load!');
      return;
    }

    const { load } = response.data;

    const [logs] = load.logs;

    loadContainer.innerHTML = `
        
       <div class="truck-item"> <p>Id:</p> <p>${load._id};</p></div>
       <div class="truck-item"><p> Name: </p> <p>${load.name}</p></div>
       <div class="truck-item"><p> Status: </p> <p>${load.status}</p></div>
       <div class="truck-item"><p> State: </p> <p>${load.state}</p></div>
       <div class="truck-item"><p>Pickup address: </p> <p>${load.pickup_address}</p></div>
       <div class="truck-item"><p> Delivery address: </p> <p>${load.delivery_address}</p></div>
       <div class="truck-item"><p>Payload:</p> <p>${load.payload}</p></div>
       <div class="truck-item"><p>Width:</p> <p>${load.dimensions.width}</p></div>
       <div class="truck-item"><p>Length:</p> <p>${load.dimensions.length}</p></div>
       <div class="truck-item"><p>Height:</p> <p>${load.dimensions.height}</p></div>
       <div class="truck-item"><p>Log message:</p> <p>${logs ? logs.message : '-'}</p></div>
       <div class="truck-item"><p>time:</p> <p>${logs ? logs.time : '-'}</p></div>
       <div class="truck-item"><p>Created by:</p> <p>${load.created_by}</p></div>
       <div class="truck-item"><p>Assigned to:</p> <p>${load.assigned_to}</p></div>
       <div class="truck-item"><p>Created date:</p> <p>${load.createdDate}</p></div>
        `;

    ChangeStateButton.addEventListener('click', async () => {
      console.log(1);
      try {
        const response = await axios.patch('http://localhost:8080/api/loads/active/state', {}, {
          headers: {
            authorization: `Bearer ${JWT}`,
          },
        });

        location.href = '/driver/activLoad.html';
        // location.href = './truckList.html';
      } catch (error) {
        console.log(error);
        alert(error);
      }
    });
  } catch (error) {
    alert(error.response.data.message);
  }
});
